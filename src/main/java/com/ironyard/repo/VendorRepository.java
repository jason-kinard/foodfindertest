package com.ironyard.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ironyard.springboot.data.Vendor;
import com.ironyard.springboot.data.VendorType;

@Repository("vendorRepository")
public interface VendorRepository extends PagingAndSortingRepository<Vendor, Long>{
	List<Vendor> findByName(String name);
	List<Vendor> findByType(VendorType type);
	List<Vendor> findByNameIgnoreCaseContaining(String name);
}
