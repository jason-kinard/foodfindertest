angular
  .module('app')
  .config([
    '$stateProvider',
    '$urlServiceProvider',
    '$locationProvider',
    ($stateProvider, $urlServiceProvider, $locationProvider) => {
      const loginState = {
        name: 'main',
        url: 'index.html',
        component: 'mainPage'
      };

      const formState = {
        name: 'form',
        url: 'form.html',
        component: 'formPage'
      };

      $stateProvider.state(loginState);
      $stateProvider.state(signUpState);

      $locationProvider.html5Mode(true);
      $urlServiceProvider.rules.otherwise({ state: 'login' });
    }
  ]);