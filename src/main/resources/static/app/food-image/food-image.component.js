class FoodImageController {
  constructor() {

  }

}

angular
  .module('app')
  .component('foodImage', {
    templateUrl: '/app/food-image/food-image.component.html',
    controllerAs: 'foodImage',
    controller: [
      () => new FoodImageController()
    ]
  });