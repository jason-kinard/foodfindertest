class MainScreenController {
  constructor() {

  }

}

angular
  .module('app')
  .component('mainScreen', {
    templateUrl: '/app/main-screen/main-screen.component.html',
    controllerAs: 'mainScreen',
    controller: [
      () => new MainScreenController()
    ]
  });