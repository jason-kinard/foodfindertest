class VendorFormController {
  constructor($http) {
	  this.name = '';
	  this.address = '';
	  this.type = 'Vegan';
	  this.rating = '1';
	  this.description = '';
	  this.imgUrl = '';
	  
	  this.$http = $http;
  }
  submitForm() {
	  // Use AJAX
	  let vendor = JSON.stringify({
			    costPerPerson: 0,
			    description: this.description,
			    id: 0,
			    imgUrl: this.imgUrl,
			    name: this.name,
			    rating: this.rating,
			    type: this.type.toUpperCase()

			  });
	  
	  this.$http
	    .post('/vendor', vendor)
	    .then(() => {
	  	  this.name = '';
		  this.address = '';
		  this.type = 'Vegan';
		  this.rating = '1';
		  this.description = '';
		  this.imgUrl = '';

	      alert('SUCCEEDED!');
	      
	    })
	    .catch(() => {

	      // Show NO LOGIN if all goes poorly.
	      alert('Error in data entry!');

	    });
  }

}

angular
  .module('app')
  .component('vendorForm', {
    templateUrl: '/app/vendor-form/vendor-form.component.html',
    controllerAs: 'vendorForm',
    controller: [
    	'$http',($http) => new VendorFormController($http)
    ]
  });