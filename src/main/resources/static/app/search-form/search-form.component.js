function getStars(num){
	var stars = "";
	for(var i = 1; i <= num; i++){
		stars += "&#9733; "
	}
	for(var i = 1; i <= 5 -num; i++){
		stars += "&#9734; "
	}
	return stars;
}


function createThumbnail(source, restaurant, stars, cost, loc, desc) {
  return '<div class="thumbnail_wrap thumbnail">' +
    '<img class="food_image" src="' + source + '" height=100% width=100% />' +
    '<div class="descrip_layer">' +
    '<p class="descrip_text" id="name"> ' + restaurant + ' </p>' +
    '<p class="descrip_text" id="stars"> ' + getStars(stars) + ' </p>' +
   // '<p class="descrip_text" id="cost"> ' + cost + ' </p>' +
    //'<p class="descrip_text" id="location"> ' + loc + ' </p>' +
    '<p class="descrip_text" id="description"> ' + desc + ' </p>' +
    '</div>' +
    '</div>';
}
function createIt(data) {
  var displayRow = "";
  var displayCol = "";
  var total = "";
  var append = false;
  for (var j = 0; j < Math.ceil(data.length / 3); j++) {
    for (var i = 0; i < 3; i++) {
      if (data[3 * j + i] != null) {
        var html = createThumbnail(data[j * 3 + i].imgUrl, data[j * 3 + i].name, data[j * 3 + i].rating, data[j * 3 + i].costPerPerson, data[j * 3 + i].address, data[j * 3 + i].description);
        displayCol += createColumn(html);
        append = true;
        //alert(displayCol)
      }
    }
    if(append) {
      displayRow = createRow(displayCol);
			total += displayRow;
    }
    append = false;
    displayRow = "";
    displayCol = "";
  }
     $('.searchResults').append(total);
}

function createColumn(data) {
  return "<div class='col-md-4'>" + data + "</div>";
}

function createRow(data) {
  return "<div class='row'>" + data + "</div>";
}
class searchController {

  // A constructor to set the initial values of
  // username and password
  constructor($http) {
    // Set a property of the controller to the value
    // of the provided service.
    this.$http = $http;
    this.searchField = '';
  }

  submitForm() {
	  // Use AJAX
	  this.$http
	    .get('/vendor/search/' + this.searchField)
	    .then((response) => {

	      // Show SUCCEEDED if all goes well!
	      
	      this.searchData = response.data;
	      $('.searchResults').empty();
	      createIt(response.data);
	    })
	    .catch(() => {

	      // Show NO LOGIN if all goes poorly.
	      alert('NO LOGIN!');

	    });
  }
}

angular
  .module('app')
  .component('searchForm', {
    templateUrl: '/app/search-form/search-form.component.html',
    controllerAs: 'search',
    controller: ['$http',($http) => new searchController($http)]
  });