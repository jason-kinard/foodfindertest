class ContactController {
  constructor() {

  }

}

angular
  .module('app')
  .component('contact', {
    templateUrl: '/app/contact-form/contact.component.html',
    controllerAs: 'contact',
    controller: [
      () => new ContactController()
    ]
  });