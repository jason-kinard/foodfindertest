package com.ironyard.springboot.data;

public enum VendorType {
	VEGAN, ITALIAN, BAR, ASIAN, BREAKFAST, AMERICAN, DESSERT, MEXICAN
}
